Advent of Code
=============================

Advent of Code has been around for a few years, this repo contains historical values as well as any new
advent of code practice problems provided.  

## References

- [Advent of code - 2019](https://adventofcode.com/2019)  
- [Advent of code - 2018](https://adventofcode.com/2018)  
- [Advent of code - 2017](https://adventofcode.com/2017)  
- [Advent of code - 2016](https://adventofcode.com/2016)  
- [Advent of code - 2015](https://adventofcode.com/2015)  
